# FLP Prolog - Turing machine simulation #

Prolog school project for FLP at FIT VUT implemented by Josef Kadleček-xkadle35.

Project implements non-deterministic Turing machine in Prolog.
Expected input: file specifying rules in form <state> <char> <newstate> <new char / action>
* States are big letters except R or L .
* Chars are small letters.
* Action is L or R.
Last line of input file is input tape.

### How do I get set up? ###

* Have SWI Prolog
* Compile using make or use interactive mode.

### How to run? ###
 * ./flp-20-log < file_with_ts_specification >output_file
 * Or interactive: swipl flp-20-log

### What happens if ... ###
* TM is in loop? Program will return false.
* TMs head falls of (without any other option to finish)? Program will return false.
* TM does not find solution? False it is.

### How to run tests? ###
* Use runTests.sh (chmod +x to make it run-able)
* Script will compare all numbered inputs in file tests to numbered outputs.
* To add test add it to "tests" folder an increase the number in runTests.sh

### What are the tests testing? ###
* 0   - TM can read and apply simple rule.
* 1   - TM can read input and apply rules even if some rules lead to dead end.
* 2   - TM can work with blank properly.
* 3   - TM can handle infinite cycles (doesn't go to same configuration twice).
* 4   - TMs had has to fall off (fail).
* 5   - TMs head DOES NOT have to fall off.
* 6   - TM is forced to cycle (fail).
* 7   - TM rules are in incorrect format (fail).
* 8   - TM does not find solution for input (fail).

### How log will these tests run? ###
* About half a second.
