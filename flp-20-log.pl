/*
FLP 2020 ProLog project by Josef Kadleček - xkadle35
*/
:- dynamic rule/4, configuration/3.

% Runs the evaluation of TM.
simulate(Tape) :-
    once(run('S', [], _, Tape, _, [])).

% Saves continuous results in variable.
logResults(L,S,Rin,Before,After):-
  reverse(Rin, R),
  append([S], L, H0),
  append(R, H0, H1),
  append(["\n"], H1, H2),
  reverse(H2, H3),
  append(Before,H3, After).

writeResults([]).
writeResults(["\n"|XS]) :- nl, writeResults(XS).
writeResults([X|XS]) :- write(X), writeResults(XS).

% Main part of algorythm which simulates the TM.
% State, Tape left side, Result tape left side, right side, result right side.
run('F', Ls, Ls, Rs, Rs, Steps) :-
  logResults(Ls,'F',Rs,Steps,StepsFinal),
  writeResults(StepsFinal),!.
run(Q, LeftSideIn, LeftSideOut, RightSideIn, RightSideOut, Steps) :-
    asserta(configuration(Q, LeftSideIn,RightSideIn)),
    logResults(LeftSideIn,Q,RightSideIn,Steps,StepsNew),
    readTape(RightSideIn, RsRest, Sym),
    rule(Q, Sym, QNext, SymOrAction),
    action(SymOrAction, Sym, LeftSideIn, LeftSideNew, RsRest, RightSideNew),
    not(configuration(QNext, LeftSideNew, RightSideNew)),
    run(QNext, LeftSideNew, LeftSideOut, RightSideNew, RightSideOut, StepsNew).

%Gets next symbol from tape.
readTape([], [], ' ').
readTape([Symbol|RightSide], RightSide, Symbol).

%Preforms given action - R, L or put char.
action('L', _, [], _, _, _) :- !, fail.
action('L', Symbol,  LeftSide, LeftSide, RightSide,  [Symbol|RightSide]).
action('R', Symbol,  LeftSideIn, [Symbol|LeftSideIn], Rs, Rs).
action( Symbol , _,  Ls, Ls, Rs, [Symbol|Rs]) :- char_type(Symbol, lower).


% *****PARSING STUFF*****
% Parses rule, last line is input tape.
parseAll([Input|[]], Input).
parseAll([Rule|Rest], Input) :- parseInputRules(Rule), parseAll(Rest, Input).

% Dynamicaly creates predicate rule/4 based on input.
% End rules (leading to F state) are before other rules to ease the calculation.
parseInputRules([State, ' ', InputChar, ' ', 'F', ' ', Action]) :-
  asserta(rule(State, InputChar, 'F', Action)).
parseInputRules([State, ' ', InputChar, ' ', NextState, ' ', Action]) :-
  assertz(rule(State, InputChar, NextState, Action)).

% Functions provided with asignment, handles parsing STDIN.
/** FLP 2020 autor: Martin Hyrs, ihyrs@fit.vutbr.cz **/
read_line(L,C) :-
	get_char(C),
	(isEOFEOL(C), L = [], !;
		read_line(LL,_),
		[C|LL] = L).

isEOFEOL(C) :-
	C == end_of_file;
	(char_code(C,Code), Code==10).

read_lines(Ls) :-
	read_line(L,C),
	( C == end_of_file, Ls = [] ;
	  read_lines(LLs), Ls = [L|LLs]
	).
% End of provided functions.

% Main for compiled version:
% Parses rules and input tape from STDIN and runs simulation.
main :-
  once(read_lines(Lines)),
  parseAll(Lines, Input),
  simulate(Input),halt
  ; write(false), nl, halt.
