
#Compile program
swipl -o flp-20-log -g main -c flp-20-log.pl 2>/dev/null
if [ $? != 0 ]
then
    echo "COMPILATION FAILED"
    exit 1
fi


function run_single {
	./flp-20-log <$IN_FILE &> testing.out
	DIFF=$(diff testing.out $REF_FILE 2>&1)
	if [ "$DIFF" != "" ]
	then
	    echo "TEST $IN_FILE FAILED"
	    echo $DIFF
	else
		echo "TEST $IN_FILE PASSED"
	fi
	rm testing.out
}

function run_all {
		for TESTNUM in {0..8}
		do
			IN_FILE="./tests/input_$TESTNUM"
			REF_FILE="./tests/output_$TESTNUM"
			run_single
		done
}

run_all
